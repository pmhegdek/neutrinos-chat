const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');

const basicRouter = require('./routes/basic-routes');
const chatAppRouter = require('./routes/chat-app-routes');
const authRouter = require('./routes/auth-routes');
let User = require('./model/user.js');
let messageReadWrite = require('./model/message-read-write');

const io = require('./socket/socket');
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cookieParser());
app.use(session({
  secret: "neutrinos-chat-app",
  saveUninitialized:true,
  resave:true
}));


//set static file path for express routes
app.use(express.static(path.join(__dirname, 'public/app')));
app.use('/', basicRouter);
app.use('/auth', authRouter);

//set static file path for angular app
app.use(express.static(path.join(__dirname, 'angular-chat-app/dist')));
app.use('/chat-app', chatAppRouter);

let liveUsersSocketMap = {};
let logedInUsers;


//socket handling
io.on('connection', (socket) => {
  console.log("User connected");

  //authorize the socket
  socket.on('authorization', (data) => {
    if (User.logedInUsers.hasOwnProperty(data.orgName) && User.logedInUsers[data.orgName].includes(data.authToken)) {

      //emit authorized to client
      socket.emit('authorized', {
        "code": 200,
        "auth": "SUCCESS"
      });

      //attach username and orgname to socket
      socket.username = data.authToken;
      socket.orgName = data.orgName;

      //add new socket to live socket mapping
      liveUsersSocketMap[data.authToken] = socket;

      //add socket to orgname room
      socket.join(data.orgName);

      //send all users to particular socket
      User.getUserByOrgName(data.orgName, function (err, data) {
        if (err) {
          console.log(err);
        } else {
          socket.emit('loged-in-users', { 'liveUsers': data });
        }
      })


      //send user's backup messages
      messageReadWrite.getMyMessages(socket.username, function (err, data) {
        if (err) {
          console.log(err);
        } else {
          socket.emit('backup-messages', { 'backupMessage': data });
        }
      });

    } else {
      socket.disconnect();
    }
  })

  //on socket disconnect
  socket.on('disconnect', () => {

    //chek value of username
    if (socket.username == undefined || socket.username == null || socket.username == "") {
      console.log("undefined socket");
      return;
    }

    //remove socket from livesocketmap
    delete liveUsersSocketMap[socket.username];

    //remove user from logedinUserarray
    User.logedInUsers[socket.orgName].splice(User.logedInUsers[socket.orgName].indexOf(socket.username), 1);

  });

  //on receiving new message from socket
  socket.on('new-text-message', (message) => {
    messageReadWrite.writeMessageToFile(message, function (err, result) {
      if (err) {
        console.log(err);
      } else {
        //erro handling
      }
    });

    if (liveUsersSocketMap.hasOwnProperty(message.to)) {
      liveUsersSocketMap[message.to].emit('message', message);
    } else {
      //socket.emit('new-text-message', 'error');
    }
    //socket.emit('message', message);
  });
});


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
