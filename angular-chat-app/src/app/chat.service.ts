import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';


import * as io from 'socket.io-client';



@Injectable()
export class ChatService {
  private socket_url = "http://localhost:3000";
  private socket;
  private rcvdMessage;
  private authToken;
  public authStatus;
  public message = new Subject();
  public logedInUsers = new Subject();
  public username;
  public orgName;
  public backupMessages = new Subject();

  constructor(private http: Http) {

  }

  init() {
    this.authStatus = new Observable((observer) => {
      observer.next(this.authStatus);
    });
  }

  //listen for message
  getMessages() {
    this.socket.on('message', (data) => {
      this.message.next(data);
    });
  }

  //listen for backup messages
  getBackupMessages() {
    this.socket.on('backup-messages', (data) => {
      this.backupMessages.next(data);
    });
  }

  //listen for loged-in users
  getLogedInUsers() {
    this.socket.on('loged-in-users', (data) => {
      this.logedInUsers.next(data);
    });
  }

  //emit message
  sendMessage(message) {
    this.socket.emit('new-text-message', message);
  }

  //get auth token to authorize socket
  getAuthToken(): void {
    this.init();
    this.http.get("/auth/token")
      .map(response => response.json()).subscribe(
      (response) => {
        if (response.authToken != null) {
          this.authToken = response.authToken;
          this.username = response.authToken;
          this.orgName = response.orgName;
          this.connectSocket();
          //return callback(this.authToken);
        } else {
          console.log("authtoken nill");
        }
      },
      function (err) {
        err => console.log("error: " + err);
      },
      function () {
        //this.connectSocket;
      });
  }

  //logout
  logout() {
    this.http.get("/auth/logout")
      .map(response => response.json()).subscribe(
      (response) => {
        window.location.href = 'http://www.localhost:3000';
      },
      function (err) {
        err => console.log("error: " + err);
      },
      function () {
        window.location.href = 'http://www.localhost:3000';
        //this.connectSocket;
      });
  }
  //conect to socket
  connectSocket() {
    this.socket = io(this.socket_url);
    this.authSocket();
  }

  authSocket(): void {
    this.socket.emit('authorization', {
      authToken: this.authToken,
      orgName: this.orgName
    });

    this.authDone().subscribe((code) => {
      if (code == 200) {
        this.authStatus = code;
        this.getMessages();
        this.getLogedInUsers();
        this.getBackupMessages();
      }
    }, (err) => {
      console.log(err);
      window.location.href = '/';
    }), () => {
      console.log("done");
    };
  }

  authDone() {
    let observable = new Observable(observer => {
      if (this.socket == null) {
        return;
      }

      this.socket.on('authorized', (data) => {
        observer.next(data.code);
      });
      return () => {
        this.socket.disconnect();
      };
    })
    return observable;
  }
}
