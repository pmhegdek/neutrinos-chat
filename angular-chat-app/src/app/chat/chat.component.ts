import { Component, OnInit, AfterViewChecked, ElementRef, ViewChild } from '@angular/core';
import { ChatService } from '../chat.service'
import { NoopAnimationsModule } from '@angular/platform-browser/animations';


import {
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule
} from '@angular/material';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@Component({
  selector: "chat-app",
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
  providers: [ChatService]
})
export class ChatComponent implements OnInit, AfterViewChecked {

  @ViewChild('scrollChat') private myScrollContainer: ElementRef;
  @ViewChild('scrollFooter') private myChatScrollContainer: ElementRef;
  @ViewChild('messageDiv') private messageDiv: ElementRef;
  private connection;
  private messageText;
  private toSendMessageObj;
  private recievedMessageObj: any;
  private liveUsers: any;
  private currentChatUser: any;
  private currentChatMessages: any;
  private userWiseAllMessages: any;
  public myname;
  public newMessageArrived: any;


  constructor(public chatService: ChatService) { }

  ngOnInit() {
    this.chatService.getAuthToken();
    //this.connection=this.chatService.initSocket();
    this.chatService.authStatus.subscribe((data) => {
      this.getMessages();
      this.getLiveUsers();
      this.getBackupMessages();
      this.scrollToBottom();

    }, (err) => {
      console.log(err);
    });
  }

  ngAfterViewChecked() {
    this.scrollToBottom();

  }

  getMessages() {

    this.chatService.message.subscribe((messageObj) => {
      this.newMessageArrived = {};
      this.newMessageArrived = messageObj;

      if (this.currentChatMessages == null || this.currentChatMessages == undefined) {
        this.currentChatMessages = [];
      }

      if (this.userWiseAllMessages == undefined) {
        this.userWiseAllMessages = {};
      }

      if (!this.userWiseAllMessages.hasOwnProperty(messageObj['from'])) {
        this.userWiseAllMessages[messageObj['from']] = [];
      }
      //if the current user and from user are same then only push new message
      if (this.currentChatUser == messageObj['from']) {
        this.currentChatMessages.push(messageObj);
      }
      this.userWiseAllMessages[messageObj['from']].push(messageObj);
      //new message received
      //this.received = value;
    });

    this.scrollToBottom();
  }

  getLiveUsers() {
    this.chatService.logedInUsers.subscribe((value) => {

      //remove my-name from live user list
      if (value['liveUsers'].includes(this.chatService.username)) {
        this.myname = this.chatService.username;
        value['liveUsers'].splice(value['liveUsers'].indexOf(this.chatService.username), 1);
      }
      this.liveUsers = value['liveUsers'];
    });
  }

  getBackupMessages() {
    this.chatService.backupMessages.subscribe((value) => {
      let data = value['backupMessage'];
      for (let k = 0; k < data.length; k++) {
        this.processReceivedMessage(data[k]);
      }
    });
  }

  sendMessage() {

    this.scrollToBottom();
    if (this.currentChatUser == undefined) {
      return;
    }

    this.toSendMessageObj = {
      "from": this.chatService.username,
      "orgName": this.chatService.orgName,
      "to": this.currentChatUser,
      "message": this.messageText,
      "time": new Date()
    }
    this.chatService.sendMessage(this.toSendMessageObj);
    if (this.userWiseAllMessages == undefined || this.userWiseAllMessages) {
      this.userWiseAllMessages = {};
    }

    if (!this.userWiseAllMessages.hasOwnProperty(this.currentChatUser)) {
      this.userWiseAllMessages[this.currentChatUser] = [];
    }

    if (this.currentChatMessages == null || this.currentChatMessages == undefined) {
      this.currentChatMessages = [];
    }

    this.userWiseAllMessages[this.currentChatUser].push(this.toSendMessageObj);
    this.currentChatMessages.push(this.toSendMessageObj);
    this.messageText = '';
    this.toSendMessageObj = new Object();
    this.scrollToBottom();
  }

  //switch chat window based on click on the user
  changeChatWindowTo(user) {
    this.currentChatUser = user;

    if (this.userWiseAllMessages == undefined || this.userWiseAllMessages == null) {
      this.userWiseAllMessages = {};
    }

    if (!this.userWiseAllMessages.hasOwnProperty(user)) {
      this.userWiseAllMessages[user] = [];
    }

    this.currentChatMessages = this.userWiseAllMessages[user];

    if (this.currentChatMessages.length == 0) {
      this.currentChatMessages = new Array();
    }

    this.scrollToBottom();

  }

  //arrange message
  processReceivedMessage(msg: any) {
    let message = JSON.parse(msg);
    if (this.userWiseAllMessages == null || this.userWiseAllMessages == undefined) {
      this.userWiseAllMessages = {};
    }
    if (message == null || message == undefined) {
      return;
    } else {
      if (message['from'] == this.chatService.username) {
        if (this.userWiseAllMessages[message['to']] == undefined || this.userWiseAllMessages[message['to']] == null) {
          this.userWiseAllMessages[message['to']] = [];
        }
        this.userWiseAllMessages[message['to']].push(message);
      } else {
        if (this.userWiseAllMessages[message['from']] == undefined || this.userWiseAllMessages[message['from']] == null) {
          this.userWiseAllMessages[message['from']] = [];
        }
        this.userWiseAllMessages[message['from']].push(message);
      }
    }
    this.scrollToBottom();
  }

  //logout
  logout() {
    this.chatService.logout();
  }
  //scroll to bottom
  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight + 100;
      this.myChatScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight + 100;
    } catch (err) {
      console.log(err);
    }
  }
}
