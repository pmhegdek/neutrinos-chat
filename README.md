# README #

This is Chat app developed in NodeJs, ExpressJs, AngularJS


### How do I get set up? ###

* install node and angular-cli
* go to to application direcory
* install required dependencies for express server using command $ sudo npm install
* install requred dependencies for angular-app, go to directory ''angular-chat-app' and issue command $ sudo npm install
* build the angular app by issuing command $ ng build
* start the server with $ sudo npm start

if there is any error (ERROR: Cannot find module 'socket.io-client') even after installing modules: then install typings for socket.io via npm i @types/socket.io-client