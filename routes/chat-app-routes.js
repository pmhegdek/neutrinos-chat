var express = require('express');
var router = express.Router();
var path = require('path');


/* GET home page. */
router.get('/', function (req, res, next) {
  //res.send('Welcome to home page');
  if (req.session.username == null || req.session.orgName == null) {
    res.status(302).redirect('/');
  } else {
    res.sendFile(path.join(__dirname, '../angular-chat-app/dist/index.html'));
  }
});

module.exports = router;
