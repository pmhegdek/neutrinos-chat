var express = require('express');
var router = express.Router();
var path = require('path');


/* GET home page. */
router.get('/', function (req, res, next) {
  //res.send('Welcome to home page');
  if (req.session.username == null || req.session.orgName == null) {
    res.render('login');
  }else{
    res.redirect('/chat-app');
  }
});

module.exports = router;
