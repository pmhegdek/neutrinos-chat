const express = require('express');
const router = express.Router();
const fileHandler = ('./model/file');
const path = require('path');
const User = require('../model/user');

router.get('/', function (req, res, next) {
  res.send('auth router');
});

router.post('/login', function (req, res) {
  User.isExists(req.body.username, req.body.orgName, function (err, result) {
    if (err) {
      res.status(404).json({
        "statusCode": 404,
        "message": "DB_ERROR"
      });
    } else {
      if (result) {
        //success
        if (!User.logedInUsers.hasOwnProperty(req.body.orgName)) {
          User.logedInUsers[req.body.orgName] = [];
        }
        User.logedInUsers[req.body.orgName].push(req.body.username);
        //User.logedInUsers.push(req.body.username);
        req.session.username = req.body.username;
        req.session.orgName = req.body.orgName;
        res.redirect('/chat-app');
      } else {
        //save user to file
        User.save(req.body.username, req.body.orgName, function (err2, result2) {
          if (err2) {
            res.status(404).json({
              "statusCode": 404,
              "message": "DB_ERROR"
            });
          } else {
            if (result2) {
              if (!User.logedInUsers.hasOwnProperty(req.body.orgName)) {
                User.logedInUsers[req.body.orgName] = [];
              }
              User.logedInUsers[req.body.orgName].push(req.body.username);
              //User.logedInUsers.push(req.body.username);
              req.session.username = req.body.username;
              req.session.orgName = req.body.orgName;
              res.status(201).json({
                "statusCode": 201,
                "message": "NEW_USER_CREATED"
              });
            } else {
              res.status(401).json({
                "statusCode": 401,
                "message": "INVALID_ORG_NAME"
              });
            }
          }
        });
      }
    }
  });
});

router.get('/token', function (req, res) {
  if (req.session.hasOwnProperty('username') && req.session.hasOwnProperty('orgName')) {
    res.status(200).json({
      "authToken": req.session.username,
      "orgName": req.session.orgName,
      "message": "AUTH_TOKEN_SUCCESSFULL"
    });
  } else {
    res.status(200).json({
      "authToken": "",
      "message": "AUTH_TOKEN_FAILED"
    });
  }
});

router.get('/logout',function(req,res){
  req.session.destroy(function(err){
    if(err){
      console.log(err);
    }else{
      res.status(200).json({
        "message":"LOGOUT_SUCCESS"
      });
    }
  });
})

module.exports = router;