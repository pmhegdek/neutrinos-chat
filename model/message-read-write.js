
const jsonfile = require('jsonfile');
const fs= require('fs');
const path = require('path');
const MESSAGES_JSON_FILE = '../data-files/messages.json';


var messageReadWrite= function(){}

messageReadWrite.writeMessageToFile= function(message,callback){
    fs.appendFile(path.join(__dirname,MESSAGES_JSON_FILE),JSON.stringify(message)+'\n',function(err){
        if(err){
            console.log(err);
            return callback(err);
        }else{
            return callback(null,true);
        }
    });
}

messageReadWrite.getMyMessages=function(username,callback){
    fs.readFile(path.join(__dirname,MESSAGES_JSON_FILE),"utf8",function(err,data){
        if(err){
            console.log("err");
            return callback(err);
        }else{
            let splitData=data.split('\n');
            let userDataArray=[];
            for(let k=0;k<splitData.length;k++){
                if(splitData[k]==undefined || splitData[k]==null || splitData[k]==''){
                    continue;
                }
                let obj= JSON.parse(splitData[k]);
                if(obj['from']== username || obj['to']== username){
                    userDataArray.push(splitData[k]);
                }
            }
            callback(null,userDataArray);
        }
    });
}

module.exports= messageReadWrite;