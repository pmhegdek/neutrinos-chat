const jsonfile = require('jsonfile');
const path = require('path');
const USERS_JSON_FILE = '../data-files/users.json';

var User = function () {

}

User.logedInUsers = {};

User.isExists = function (username, orgName, callback) {
    jsonfile.readFile(path.join(__dirname, USERS_JSON_FILE), function (err, users) {
        if (err) {
            console.log("err:" + err);
            return callback(err);
        } else {
            if (users.hasOwnProperty(username)) {
                if (users[username].orgName == orgName) {
                    return callback(null, true);
                } else {
                    return callback(null, false);
                }
            } else {
                return callback(null, false);
            }
        }
    })
};

User.getUserByOrgName=function(orgname,callback){
    let users = jsonfile.readFileSync(path.join(__dirname, USERS_JSON_FILE));
    let OrgUsersArray=[];
    for(user in users){
        if(users[user].orgName==orgname){
            OrgUsersArray.push(users[user].id);
        }
    }
    callback(null, OrgUsersArray);
};
User.save = function (username, orgName, callback) {

    let users = jsonfile.readFileSync(path.join(__dirname, USERS_JSON_FILE));

    if (users.hasOwnProperty(username)) {
        return callback(null, false);
    } else {
        users[username] = {
            id: username,
            orgName: orgName
        }

        jsonfile.writeFile(path.join(__dirname, USERS_JSON_FILE), users, function (err) {
            if (err) {
                return callback(err);
            } else {
                return callback(null, true);
            }
        });
    }
}


module.exports = User;
